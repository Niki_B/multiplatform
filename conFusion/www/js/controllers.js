//---------------------------------------------------------------------------------------
angular.module('conFusion.controllers', ['conFusion.services'] )
//---------------------------------------------------------------------------------------
.controller( 'AppCtrl',
//---------------------------------------------------------------------------------------
function( $scope, $ionicModal, $timeout, $localStorage )
{

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.loginData = $localStorage.retrieveObject('userinfo',{} );
  $scope.reservationData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl(
    'templates/login.html',
    {
      scope: $scope
    } )
  .then( function( modal )
  {
    $scope.modalLogin = modal;
  } );


  // Triggered in the login modal to close it
  $scope.closeLogin = function()
  {
    $scope.modalLogin.hide();
  };


  // Open the login modal
  $scope.login = function()
  {
    $scope.modalLogin.show();
  };


  // Perform the login action when the user submits the login form
  $scope.doLogin = function()
  {
    console.log('Doing login', $scope.loginData);
    $localStorage.storeObject('userinfo',$scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(
      function()
      {
        $scope.closeLogin();
      },
      1000 );
  };


  $ionicModal.fromTemplateUrl(
    'templates/reservation.html',
    {
      scope: $scope
    } )
  .then( function( modal )
  {
    $scope.modalReservation = modal;
  } );


  $scope.reserve = function()
  {
    $scope.modalReservation.show();
  };


  $scope.closeReservation = function()
  {
    $scope.modalReservation.hide();
  };


  $scope.doReservation = function()
  {
    console.log( 'Doing reservation', $scope.reservationData );

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(
      function()
      {
        $scope.closeReservation();
      },
      1000 );
  };

} )


//--------------------------------------------------------------------------------------
.controller( "MenuController",
//--------------------------------------------------------------------------------------
[ '$scope', 'dishes', 'FavoriteFactory', 'baseUrl', '$ionicListDelegate',
//--------------------------------------------------------------------------------------
function( $scope, dishes, favoriteData, baseUrl, $ionicListDelegate )
//--------------------------------------------------------------------------------------
{
  $scope.baseUrl = baseUrl;
  $scope.dishes = dishes;

  $scope.tab = 1;
  $scope.filtText = '';
  $scope.showDetails = false;


  $scope.toggleDetails = function()
  {
    $scope.showDetails = !$scope.showDetails;
  };

  $scope.select = function( setTab )
  {
    $scope.tab = setTab;

    if (setTab === 2)
    {
      $scope.filtText = "appetizer";
    }
    else if (setTab === 3)
    {
      $scope.filtText = "mains";
    }
    else if (setTab === 4)
    {
      $scope.filtText = "dessert";
    }
    else
    {
      $scope.filtText = "";
    }
  };

  $scope.isSelected = function( checkTab )
  {
    return ($scope.tab === checkTab);
  };

  $scope.addFavorite = function( index )
  {
    console.log( "index is " + index );
    favoriteData.addToFavorites(index);
    $ionicListDelegate.closeOptionButtons();
  };

}] )



//--------------------------------------------------------------------------------------
.controller('DishDetailController',
//--------------------------------------------------------------------------------------
[ '$scope', 'resolvedDish', 'FavoriteFactory', '$stateParams',  '$ionicPopover', '$ionicModal',
  'baseUrl', '$ionicLoading', '$timeout',
//--------------------------------------------------------------------------------------
function( $scope, resolvedDish, favoriteData, $stateParams, $ionicPopover, $ionicModal, baseUrl,
          $ionicLoading, $timeout )
//--------------------------------------------------------------------------------------
{
  $scope.baseUrl = baseUrl;
  $scope.showDetails = false;
  $scope.detailsMessage = "Loading...";
  $scope.popover = undefined;

  $ionicPopover.fromTemplateUrl(
    'templates/dishdetail-popover.html',
    {
      scope: $scope
    } )
  .then(
    function( popover )
    {
      $scope.popover = popover;
    } );

  $scope.dish = resolvedDish;
  $scope.sortCriterium = "";

  $scope.openAddStuffPopOver = function( $event )
  {
    $scope.popover.show( $event );
  };

  $scope.closePopover = function()
  {
    $scope.popover.hide();
  };

  //Cleanup the popover when we're done with it!
  $scope.$on(
    '$destroy',
    function()
    {
      $scope.popover.remove();
    } );

  $scope.addThisToFavorites = function()
  {
    favoriteData.addToFavorites( $scope.dish.id );
    $scope.closePopover();
  };

  $scope.addNewComment = function()
  {
    $scope.closePopover();
    $scope.openCommentForm();
  };

  $ionicModal.fromTemplateUrl(
    'templates/dishcomment.html',
    {
      scope: $scope
    } )
  .then( function( modal )
  {
    $scope.modalComment = modal;
  } );


  // Triggered in the login modal to close it
  $scope.closeCommentForm = function()
  {
    $scope.modalComment.hide();
  };


  // Open the login modal
  $scope.openCommentForm = function()
  {
    $scope.modalComment.show();
  };

}] )



//--------------------------------------------------------------------------------------
.controller( "DishCommentController",
//--------------------------------------------------------------------------------------
[ '$scope', 'MenuFactory',
//--------------------------------------------------------------------------------------
function( $scope, menuData )
//--------------------------------------------------------------------------------------
{
  $scope.stars = [ 1, 2, 3, 4, 5 ];
  $scope.feedback =
    {
      rating:5,
      comment:"",
      author:"",
      date:""
    };

  $scope.submitComment = function()
  {
    $scope.feedback.date = new Date().toISOString();
    $scope.dish.comments.push( $scope.feedback );
    menuData.update( { id: $scope.dish.id },$scope.dish );
    $scope.feedback =
      {
        rating:5,
        comment:"",
        author:"",
        date:""
      };
    $scope.closeCommentForm(); // comming from the parent controller
  };

}] )



//--------------------------------------------------------------------------------------
.controller( 'FavoritesController',
//--------------------------------------------------------------------------------------
[ '$scope', 'resolvedDishes', 'resolvedFavorites', 'FavoriteFactory', 'baseUrl', '$ionicListDelegate', '$ionicPopup',
//--------------------------------------------------------------------------------------
function( $scope, resolvedDishes, resolvedFavorites, favoriteFactory, baseUrl, $ionicListDelegate, $ionicPopup )
//--------------------------------------------------------------------------------------
{
  $scope.baseUrl = baseUrl;
  $scope.shouldShowDelete = false;
  $scope.favorites = resolvedFavorites;
  $scope.tab = 1;
  $scope.filtText = '';

  $scope.dishes = resolvedDishes;

  $scope.toggleDelete = function()
  {
    $scope.shouldShowDelete = !$scope.shouldShowDelete;
    console.log( $scope.shouldShowDelete );
  };

  $scope.deleteFavorite = function( index )
  {
    var confirmPopup = $ionicPopup.confirm(
      {
        title: 'Confirm delete',
        template: 'Are you sure you want to delete this item?'
      } )
    .then( function( res )
      {
        if( res )
        {
          console.log( 'OK to delete' );
          favoriteFactory.deleteFromFavorites( index );
        }
        else
        {
          console.log( 'Canceled deletion' );
        }
      } );

      $scope.shouldShowDelete = false;
  };

  $scope.select = function( setTab )
  {
    $scope.tab = setTab;

    if (setTab === 2)
    {
      $scope.filtText = "appetizer";
    }
    else if (setTab === 3)
    {
      $scope.filtText = "mains";
    }
    else if (setTab === 4)
    {
      $scope.filtText = "dessert";
    }
    else
    {
      $scope.filtText = "";
    }
  };

  $scope.isSelected = function( checkTab )
  {
    return ($scope.tab === checkTab);
  };

}] )


//--------------------------------------------------------------------------------------
.controller( "IndexController",
//--------------------------------------------------------------------------------------
[ '$scope', 'MenuFactory', 'PromotionFactory', 'CorporateFactory', '$filter', 'baseUrl',
//--------------------------------------------------------------------------------------
function( $scope, menuInfo, promotionInfo, corporateInfo, filterFunction, baseUrl )
//--------------------------------------------------------------------------------------
{
  $scope.baseUrl = baseUrl;
  $scope.showExecutiveChef = false;
  $scope.executiveChefMessage = "Loading...";
  $scope.theChief = corporateInfo.query(
    function( response )
    {
      var leaderlist = response;
      $scope.theChief = filterFunction('filter')( leaderlist , {abbr: "EC"}, true )[0];
      $scope.showExecutiveChef = true;
    },
    function( response )
    {
      $scope.executiveChefMessage = "Error: " + response.status + " " + response.statusText;
    } );

  $scope.showFood = false;
  $scope.foodMessage = "Loading...";
  $scope.firstDish = {};
  menuInfo.query(
    function( response )
    {
      $scope.dishes = response;
      $scope.firstDish = $scope.dishes[0];
      $scope.showFood = true;
    },
    function( response )
    {
      $scope.foodMessage = "Error: " + response.status + " " + response.statusText;
    } );

  $scope.showPromos = false;
  $scope.promosMessage = "Loading...";
  $scope.promotions = promotionInfo.query(
    function( response)
    {
      $scope.promotions = response;
      $scope.showPromos = true;
    },
    function( response )
    {
      $scope.promosMessage = "Error: " + response.status + " " + response.statusText;
    } );

}] )



//--------------------------------------------------------------------------------------
.controller( "AboutController",
//--------------------------------------------------------------------------------------
[ '$scope', 'CorporateFactory', 'baseUrl',
//--------------------------------------------------------------------------------------
function( $scope, corporateInfo, baseUrl )
//--------------------------------------------------------------------------------------
{
  $scope.baseUrl = baseUrl;
  $scope.leaders = corporateInfo.query(
    function( response)
    {
      $scope.leaders = response;
      $scope.showLeaders = true;
    },
    function( response )
    {
      $scope.showLeadersMessage = "Error: " + response.status + " " + response.statusText;
    } );
  $scope.showLeaders = false;
  $scope.showLeadersMessage = "Loading...";
}] )



//--------------------------------------------------------------------------------------
.controller( "ContactController",
//--------------------------------------------------------------------------------------
[ '$scope',
//--------------------------------------------------------------------------------------
function( $scope )
//--------------------------------------------------------------------------------------
{
  $scope.feedback = 
    {
      mychannel:"",
      firstName:"",
      lastName :"",
      agree    :false,
      email    :"",
      comments :""
    };

  var channels = 
    [
      {
        value:"tel",
        label:"Tel."
      },
      {
        value:"Email",
        label:"Email"
      }
    ];
  $scope.channels = channels;

  $scope.invalidChannelSelection = false;
}] )



//--------------------------------------------------------------------------------------
.controller( "FeedbackController",
//--------------------------------------------------------------------------------------
[ '$scope', 'FeedbackFactory',
//--------------------------------------------------------------------------------------
function( $scope, feedbackService )
//--------------------------------------------------------------------------------------
{
  $scope.sendFeedback = function()
  {
    if( $scope.feedback.agree && ($scope.feedback.mychannel === "") )
    {
      $scope.invalidChannelSelection = true;
      console.log('incorrect');
    }
    else
    {
      $scope.invalidChannelSelection = false;
      console.log( $scope.feedback );
      feedbackService.feedbackSender().save( $scope.feedback );
      $scope.feedback =
        {
          mychannel:"",
          firstName:"",
          lastName:"",
          agree:false,
          email:"",
          comments: ""
        };
      $scope.feedback.mychannel="";
      $scope.feedbackForm.$setPristine();
    }
  };
}] )


//---------------------------------------------------------------------------------------
.filter( 'favoriteFilter',
//---------------------------------------------------------------------------------------
function ()
//---------------------------------------------------------------------------------------
{
  return function (dishes, favorites)
  {
    var out = [];
    for (var i = 0; i < favorites.length; i++)
    {
      for (var j = 0; j < dishes.length; j++)
      {
        if( dishes[j].id === favorites[i].id )
        {
          out.push(dishes[j]);
        }
      }
    }

    return out;
  }
} )


//---------------------------------------------------------------------------------------
;
//---------------------------------------------------------------------------------------
